public class MyDate {
    int day, month, year;

    int[] maxDays = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String toString() {
        return year + "-" + (month < 10 ? "0" : "") + month + "-" + (day < 10 ? "0" : "") + day;
    }

    public void incrementDay() {
        day++;
        if (day > maxDays[month - 1]) {
            day = 1;
            incrementDay();
        } else if (month == 2 && day == 29 && !inLeapYear()) {
            day = 1;
            month++;
        }
    }

    public boolean inLeapYear() {
        return year % 4 == 0;
    }

    public void incrementYear(int diff) {
        year += diff;
        if (month == 2 && day == 29 && !inLeapYear()) {
            day = 28;
        }
    }

    public void decrementDay() {
        day--;
        if (day == 0) {
            decrementMonth();
            if (!inLeapYear() && month == 2) {
                day = 28;
            } else {
                day = maxDays[month - 1];
            }
        }
    }

    public void decrementYear() {
        incrementYear(-1);

    }

    public void decrementMonth() {
        month--;

    }

    public void incrementDay(int diff) {
        while (diff > 0) {
            incrementDay();
            diff--;
        }

    }

    public void incrementMonth(int diff) {
        month += diff;
        int yearDiff = (month - 1) / 12;

        int newMonth = ((month - 1) % 12) + 1;
        if (newMonth < 0)
            yearDiff--;
        year += yearDiff;
        month = newMonth < 0 ? newMonth + 12 : newMonth;

        if (day > maxDays[month - 1]) {
            day = maxDays[month - 1];

        }
        if (month == 2 && day == 29 && !inLeapYear()) {
            day = 28;
        }
    }

    public void decrementMonth(int diff) {
        incrementMonth(-diff);
    }

    public void decrementDay(int diff) {
        while (diff > 0) {
            decrementDay();
            diff--;
        }
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public void decrementYear(int diff) {
        while (diff > 0) {
            decrementYear();
            diff--;
        }
        if (this.year % 4 == 0 && this.month == 2) {
            this.day = 29;

        }
    }

    public boolean isBefore(MyDate date2) {
        if (date2.year > year)
            return true;
        else if (date2.year < year) return false;
        else {
            if (date2.month > month) return true;
            else if (date2.month < month) return false;
            if (date2.day > day) return true;
            else return false;
        }
    }

    public boolean isAfter(MyDate date2) {
        if (isBefore(date2)) {
            return false;
        } else return true;
    }

    public int dayDifference(MyDate date2) {
        int counter = 0;
        MyDate tempDate = new MyDate(day,month,year);
        if (isBefore(date2)){
            while(tempDate.isBefore(date2)){
                tempDate.incrementDay();
                counter++;
            }
            }
        else {
            while(tempDate.isAfter(date2)){
                tempDate.decrementDay();
                counter++;
            }




        }


        return counter;
    }
}

public class MyDateTime {
    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public String toString() {
        return date + " " + time;
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
        int dayDiff = time.incrementHour(1);
        date.incrementDay(dayDiff);
    }

    public void incrementHour(int diff) {
        int dayDiff = time.incrementHour(diff);
        if (dayDiff < 0) {
            date.decrementDay(-dayDiff);
        } else {
            date.incrementDay(dayDiff);
        }
    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {
        int dayDiff = time.incrementMinute(diff);
        if (dayDiff < 0) {
            date.decrementDay(-dayDiff);
        } else {
            date.incrementDay(dayDiff);
        }
    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementDay(int diff) {
        date.incrementDay(diff);
    }

    public void decrementMonth(int diff) {
        date.decrementMonth(diff);
    }

    public void decrementDay(int diff) {
        date.decrementDay(diff);
    }

    public void incrementMonth(int diff) {
        date.incrementMonth(diff);
    }

    public void decrementYear(int diff) {
        date.decrementYear(diff);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void incrementYear() {
        date.incrementYear();
    }


    public boolean isBefore(MyDateTime anotherDateTime) {
        long a = Long.parseLong(toString().replaceAll("-", "").toString().replaceAll(":", "").toString().replaceAll(" ", ""));
        long b = Long.parseLong(anotherDateTime.toString().replaceAll("-", "").toString().replaceAll(":", "").toString().replaceAll(" ", ""));

        return a < b;

    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        return !isBefore(anotherDateTime);
    }

//    public String dayTimeDifference(MyDateTime anotherDateTime) {
//        int diff = 0;
//        MyDateTime mdt = new MyDateTime(date, time);
//        if (isBefore(anotherDateTime)) {
//            while (mdt.isBefore(anotherDateTime)) {
//                mdt.incrementHour();
//                diff += 1;
//            }
//        } else if (isAfter(anotherDateTime)) {
//            while (mdt.isAfter(anotherDateTime)) {
//                mdt.decrementHour(1);
//                diff += 1;
//            }
//        }
//     return diff +" hour(s)";
//    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {
        int diff = 0;

        if (isBefore(anotherDateTime)) {
            MyDateTime mdt = new MyDateTime(date,time);
            while (mdt.isBefore(anotherDateTime)) {
                mdt.incrementMinute(1);
                diff += 1;
            }
        } else if (isAfter(anotherDateTime)) {
            MyDateTime mdt = new MyDateTime(date,time);
            while (mdt.isAfter(anotherDateTime)) {
                mdt.decrementMinute(1);
                diff += 1;
            }
        }
        int day = diff / 1440;
        int remain = diff % 1440;
        int hour = remain / 60;
        int minute = remain % 60;
        if(day >= 1)return day + " days " + hour + " hours " + minute + " minutes";
        else return hour + " hours";
    }


}



import static java.awt.PageAttributes.MediaType.C1;

public class Circle {
    static final double pi=3.14;
    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public double area(){
        return pi * radius * radius;
    }
    public double perimeter(){
        return 2*pi*radius;
    }
    public boolean intersect(Circle c1){

        return radius + c1.radius >= center.distanceFromPoint(c1.center);

    }
}
